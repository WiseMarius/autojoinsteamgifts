﻿using System;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using AutoJoinSteamgifts.Properties;
using AutoJoinSteamgifts.Utils.Browsers;

namespace AutoJoinSteamgifts.Managers
{
    class CookiesManager
    {
        private static readonly BrowserCookiesManager _browserCookiesManager = new BrowserCookiesManager();

        private const int InternetCookieHttponly = 0x2000;

        public static CookieContainer GetCookies()
        {
            var target = new Uri(Constants.SteamgiftsUrl);
            var cookies = new CookieContainer();

            cookies.Add(new Cookie("PHPSESSID", Settings.Default.PHPSESSID) { Domain = target.Host });
            //cookies.Add(new Cookie("_ga", Settings.Default._ga) { Domain = target.Host });
            //cookies.Add(new Cookie("_gat", Settings.Default._gat) { Domain = target.Host });
            //cookies.Add(new Cookie("_gid", Settings.Default._gid) { Domain = target.Host });

            return cookies;
        }

        public static void GetFreshCookies()
        {
            //var cookieContainer = GetCookieContainer();
            //var target = new Uri(Constants.SteamgiftsUrl);
            //var cookies = cookieContainer.GetCookies(target);

            Settings.Default.PHPSESSID = _browserCookiesManager.GetCookies().Item2;
            //Settings.Default._ga = cookies["_ga"]?.Value;
            //Settings.Default._gat = cookies["_gat"]?.Value;
            //Settings.Default._gid = cookies["_gid"]?.Value;

            Settings.Default.Save();
        }

        private static CookieContainer GetCookieContainer()
        {
            var uri = new Uri(Constants.SteamgiftsUrl);
            var datasize = 8192 * 16;
            var cookieData = new StringBuilder(datasize);
            var cookies = new CookieContainer();

            if (!InternetGetCookieEx(uri.ToString(), null, cookieData, ref datasize, InternetCookieHttponly, IntPtr.Zero))
            {
                if (datasize < 0)
                {
                    return null;
                }

                cookieData = new StringBuilder(datasize);

                if (!InternetGetCookieEx(uri.ToString(), null, cookieData, ref datasize, InternetCookieHttponly, IntPtr.Zero))
                {
                    return null;
                }
            }

            if (cookieData.Length > 0)
            {
                cookies = new CookieContainer();
                cookies.SetCookies(uri, cookieData.ToString().Replace(';', ','));
            }

            return cookies;
        }

        [DllImport("wininet.dll", SetLastError = true)]
        public static extern bool InternetGetCookieEx(string url, string cookieName, StringBuilder cookieData, ref int size, int dwFlags, IntPtr lpReserved);
    }
}
