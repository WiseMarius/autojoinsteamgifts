﻿using AutoJoinSteamgifts.Properties;

namespace AutoJoinSteamgifts.Utils.Steamgifts
{
    class SteamgiftsProfileUtils : SteamgiftsCommonUtils
    {
        public SteamgiftsProfileUtils()
        {
            PageAddress = Settings.Default.ProfileLink;
        }

        public string GetUsername()
        {
            return HtmlDocument.DocumentNode.SelectSingleNode("//div[@class='featured__heading__medium']").InnerText;
        }

        public string GetGiftsWon()
        {
            return HtmlDocument.DocumentNode.SelectSingleNode("//a[@href='/user/" + GetUsername() + "/giveaways/won']").InnerText;
        }

        public string GetGiveawaysEntered()
        {
            return HtmlDocument.DocumentNode.SelectSingleNode("(//div[@class='featured__table__row__right'])[5]").InnerText;
        }
    }
}
