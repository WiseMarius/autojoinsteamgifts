﻿using System;

namespace AutoJoinSteamgifts.Utils.Browsers
{
    interface IBrowserCookies
    {
        Tuple<string, string> GetCookies();
    }
}
