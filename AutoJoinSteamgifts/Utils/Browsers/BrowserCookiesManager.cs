﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AutoJoinSteamgifts.Utils.Browsers
{
    class BrowserCookiesManager
    {
        private static readonly List<IBrowserCookies> _browserCookies = new List<IBrowserCookies>() { new ChromeCookies() };

        public Tuple<string, string> GetCookies()
        {
            Tuple<string, string> result;
            foreach(var browser in _browserCookies)
            {
                result = browser.GetCookies();

                if(result.Item1 != string.Empty)
                {
                    return result;
                }
            }

            return Tuple.Create(string.Empty, string.Empty);
        }
    }
}
