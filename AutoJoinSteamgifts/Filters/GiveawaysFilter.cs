﻿using System;
using System.IO;
using System.Linq;
using AutoJoinSteamgifts.Properties;
using AutoJoinSteamgifts.Utils.Steamgifts;
using HtmlAgilityPack;

namespace AutoJoinSteamgifts.Filters
{
    class GiveawaysFilter : SteamgiftsCommonUtils
    {
        protected GiveawaysFilter()
        {

        }

        protected bool MeetsAllRequirements(HtmlNode giveawayNode)
        {
            if (IsEnoughTime(giveawayNode) && !IsEntered(giveawayNode) && (IsHighChanceWinning(giveawayNode) || IsExpensive(giveawayNode)))
            {
                return true;
            }

            return false;
        }

        protected static long GetTimeleftInSeconds(HtmlNode node)
        {
            var timeleft = int.Parse(node.SelectSingleNode(".//span[@data-timestamp]").Attributes[0].Value);
            var origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Local);
            origin = origin.AddSeconds(timeleft);
            var result = origin - DateTime.UtcNow;
            var res = Convert.ToInt64(Math.Floor(result.TotalSeconds));

            return (long)(res - Settings.Default.MinIntervalGiveaways);
        }

        private bool IsEntered(HtmlNode giveawayNode)
        {
            File.WriteAllText(@"D:\test.txt", giveawayNode.ToString());

            return giveawayNode.SelectSingleNode(".//div[@class='giveaway__row-inner-wrap is-faded']") != null;
        }


        private bool IsExpensive(HtmlNode giveawayNode)
        {
            var price = new string(giveawayNode.SelectSingleNode(".//span[@class='giveaway__heading__thin']").InnerText.ToCharArray().Where(char.IsDigit).ToArray());

            return int.Parse(price) >= Settings.Default.CheapestPriceToEnter;
        }

        private bool IsHighChanceWinning(HtmlNode giveawayNode)
        {
            return giveawayNode.SelectSingleNode(".//span[@class='giveaway__heading__thin']").InnerText.Contains("Copies");
        }

        private bool IsEnoughTime(HtmlNode giveawayNode)
        {
            return GetTimeleftInSeconds(giveawayNode) >
                   Settings.Default.MinIntervalGiveaways + Constants.MarginOfErrorInSeconds;
        }
    }
}
