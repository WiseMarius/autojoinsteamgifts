﻿using System.Collections.Generic;

namespace AutoJoinSteamgifts.Models
{
    class Giveaway
    {
        public string Name { get; set; }
        public string Link { get; set; }

        public Dictionary<string, string> Headers { get; set; } = new Dictionary<string, string>()
        {
            {"xsrf_token", ""},
            {"do", "entry_insert"},
            {"code", ""}
        };

        public long TimeToEnter { get; set; }
        public int Points { get; set; }
    }
}
