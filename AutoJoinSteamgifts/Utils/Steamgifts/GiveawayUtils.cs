﻿using System;
using System.Windows.Forms;
using AutoJoinSteamgifts.Models;
using NLog;
using RestSharp;
using RestSharp.Deserializers;

namespace AutoJoinSteamgifts.Utils.Steamgifts
{
    class GiveawayUtils : PageUtils
    {
        public event EventHandler FailedToEnterGiveaway;
        public event EventHandler NewLogMessage;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public GiveawayUtils()
        {

        }

        public void EnterGiveaway(Giveaway giveaway)
        {
            PageAddress = Constants.SteamgiftsUrl.Remove(Constants.SteamgiftsUrl.Length - 1, 1) + giveaway.Link;
            RefreshPage();

            if (IsAvailable())
            {
                AddHeaders(ref giveaway);

                var client = new RestClient("https://www.steamgifts.com/ajax.php")
                {
                    UserAgent =
                        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36",
                    FollowRedirects = true,
                    CookieContainer = Managers.CookiesManager.GetCookies()
                };

                var request = new RestRequest(String.Empty, Method.POST);
                request.AddParameter("xsrf_token", giveaway.Headers["xsrf_token"]);
                request.AddParameter("code", giveaway.Headers["code"]);
                request.AddParameter("do", giveaway.Headers["do"]);

                var response = client.Execute(request);

                if (!IsResponseSuccess(response))
                {
                    OnFailedToEnterGiveaway(new EventArgs());
                }
            }
            else
            {
                Logger.Trace("Could not enter! Code: " + giveaway.Headers["code"] + ". You probably won this game recently.");

                OnFailedToEnterGiveaway(new EventArgs());
            }
        }

        private bool IsResponseSuccess(IRestResponse response)
        {
            var jsonDeserializer = new JsonDeserializer();
            var jsonResponse = jsonDeserializer.Deserialize<Response>(response);

            Logger.Trace("Response: " + jsonResponse.type);
            OnNewLogMessage();

            return jsonResponse.type == "success";
        }

        private void AddHeaders(ref Giveaway giveaway)
        {
            giveaway.Headers["xsrf_token"] = HtmlDocument.DocumentNode.SelectSingleNode("//input[@name='xsrf_token']").Attributes[2].Value;
            giveaway.Headers["code"] = HtmlDocument.DocumentNode.SelectSingleNode("//input[@name='code']").Attributes[2].Value;
        }

        private bool IsAvailable()
        {
            var htmlPage = GetPage(PageAddress);

            return htmlPage.DocumentNode.SelectSingleNode(".//div[@class='sidebar__entry-insert']") != null;
        }

        protected virtual void OnFailedToEnterGiveaway(EventArgs e)
        {
            FailedToEnterGiveaway?.Invoke(this, e);
        }

        protected virtual void OnNewLogMessage()
        {
            NewLogMessage?.Invoke(this, EventArgs.Empty);
        }
    }
}
