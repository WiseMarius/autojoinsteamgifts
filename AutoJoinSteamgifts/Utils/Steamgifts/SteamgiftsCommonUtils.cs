﻿using System;
using System.Linq;

namespace AutoJoinSteamgifts.Utils.Steamgifts
{
    class SteamgiftsCommonUtils : PageUtils
    {
        public string GetProfileLink()
        {
            return Constants.SteamgiftsUrl + HtmlDocument.DocumentNode.SelectSingleNode("//a[@class='nav__avatar-outer-wrap']").GetAttributeValue("href", "");
        }

        public short NewWonGiveawaysCount()
        {
            var notificationNode = HtmlDocument.DocumentNode.SelectSingleNode(
                "//div[@class='nav__button-container nav__button-container--notification nav__button-container--active']");

            if (notificationNode == null) return 0;

            var oldNotificationNode = notificationNode.SelectSingleNode(".//div[@class='nav__notification']");
            if (oldNotificationNode != null) return Convert.ToInt16(oldNotificationNode.InnerHtml);
            
            var recentNotificationNode = notificationNode.SelectSingleNode(".//div[@class='nav__notification fade_infinite']");
            return (short) (recentNotificationNode != null ? Convert.ToInt16(recentNotificationNode.InnerHtml) : 0);
        }
    }
}
