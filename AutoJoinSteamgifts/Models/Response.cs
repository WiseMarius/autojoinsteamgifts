﻿namespace AutoJoinSteamgifts.Models
{
    class Response
    {
        // ReSharper disable once InconsistentNaming
        public string type { get; set; }
        // ReSharper disable once InconsistentNaming
        public string entry_count { get; set; }
        // ReSharper disable once InconsistentNaming
        public string points { get; set; }
    }
}
