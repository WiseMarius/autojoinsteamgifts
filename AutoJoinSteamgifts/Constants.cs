﻿namespace AutoJoinSteamgifts
{
    class Constants
    {
        public const string SteamgiftsUrl = "https://www.steamgifts.com/";

        public const string SteamgiftsUrlLogin =
                "https://steamcommunity.com/openid/login?openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.mode=checkid_setup&openid.return_to=https%3A%2F%2Fwww.steamgifts.com%2F%3Flogin&openid.realm=https%3A%2F%2Fwww.steamgifts.com&openid.ns.sreg=http%3A%2F%2Fopenid.net%2Fextensions%2Fsreg%2F1.1&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select"
            ;

        public const string SteamgiftsWonPageUrl = "https://www.steamgifts.com/giveaways/won";
        public const string SteamgiftsPageSufix = "giveaways/search?page=";
        public const int CheapestPriceToEnter = 20;
        public const int MinIdleSecondsThreshold = 5;
        public const int MaxIdleSecondsThreshold = 300;
        public const int MarginOfErrorInSeconds = 5;
    }
}
