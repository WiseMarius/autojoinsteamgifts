﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using AutoJoinSteamgifts.Models;
using AutoJoinSteamgifts.Properties;
using AutoJoinSteamgifts.Utils.Steamgifts;
using NLog;
using SteamStoreApi;

namespace AutoJoinSteamgifts.Windows
{
    public partial class MainWindow : Form
    {
        private readonly SteamgiftsUtils _steamgiftsUtils = new SteamgiftsUtils();
        private readonly SteamgiftsProfileUtils _steamgiftsProfileUtils = new SteamgiftsProfileUtils();
        private readonly GiveawayUtils _giveawayUtils = new GiveawayUtils();
        private Giveaway _giveawayToEnter;
        private readonly Random _random = new Random();
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private long _randomTimeleft;

        public MainWindow()
        {
            InitializeComponent();

            WindowState = FormWindowState.Minimized;
            _giveawayUtils.FailedToEnterGiveaway += GiveawayUtilsOnFailedToEnterGiveaway;
            _giveawayUtils.NewLogMessage += GiveawayUtilsOnNewLogMessage;
            timerRestart.Interval = (int)(Settings.Default.IdleTime * 1000 * 3600);

            backgroundWorkerRefreshPages.RunWorkerAsync();
        }

        private void backgroundWorkerRefreshPages_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            _steamgiftsUtils.RefreshPage();
            _steamgiftsProfileUtils.RefreshPage();
        }

        private void backgroundWorkerRefreshPages_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            labelPointsVaue.Text = _steamgiftsUtils.GetPoints();
            linkLabelName.Text = _steamgiftsProfileUtils.GetUsername();

            SetSpinnersVisiblity(false);
            SetLabelsVisibility(true);

            backgroundWorkerGetNextGiveaway.RunWorkerAsync();
        }

        private void SetLabelsVisibility(bool value)
        {
            labelGiveawayValue.Visible = value;
            labelPointsVaue.Visible = value;
            linkLabelName.Visible = value;
            linkLabelGifts.Visible = value;
            labelTimeValue.Visible = value;
            labelStatusValue.Visible = value;
        }

        private void SetSpinnersVisiblity(bool value)
        {
            pictureBoxSpinner1.Visible = value;
            pictureBoxSpinner2.Visible = value;
            pictureBoxSpinner3.Visible = value;
            pictureBoxSpinner4.Visible = value;
            pictureBoxSpinner5.Visible = value;
            pictureBoxSpinner6.Visible = value;
        }

        private void backgroundWorkerGetNextGiveaway_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            _giveawayToEnter = _steamgiftsUtils.GetNextGiveaway();
        }

        private void backgroundWorkerGetNextGiveaway_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            labelGiveawayValue.Text = _giveawayToEnter.Name;

            CheckNewGifts();

            var points = int.Parse(labelPointsVaue.Text);

            if (points >= Settings.Default.CheapestPriceToEnter && points >= _giveawayToEnter.Points)
            {
                _randomTimeleft = _random.Next((int)Settings.Default.MinIntervalGiveaways, (int)Math.Min(Settings.Default.MaxIntervalGiveaways, Convert.ToInt32(_giveawayToEnter.TimeToEnter)));

                labelTimeValue.Text = $@"{_randomTimeleft / 60}:{_randomTimeleft % 60}";

                timerEnterNextGiveaway.Start();

                Logger.Trace("Points: " + points + " --- Giveaway to enter: " + _giveawayToEnter.Name + " --- Cost: " + _giveawayToEnter.Points);
                InitLogger();
            }
            else
            {
                if (points < Settings.Default.CheapestPriceToEnter)
                {
                    StopWithReason(@"Not enough points [" + points + "/" + Settings.Default.CheapestPriceToEnter + "]");
                }

                if (points >= Settings.Default.CheapestPriceToEnter && points < _giveawayToEnter.Points)
                {
                    StopWithReason(@"Giveaway is too expensive [" + points + "/" + _giveawayToEnter.Points + "]");
                }

                if (!timerRestart.Enabled)
                {
                    timerRestart.Start();
                }
            }
        }

        private void timerEnterNextGiveaway_Tick(object sender, EventArgs e)
        {
            _randomTimeleft--;

            if (_randomTimeleft <= 0)
            {
                timerEnterNextGiveaway.Stop();
                backgroundWorkerEnterNextGiveaway.RunWorkerAsync();
            }

            labelTimeValue.Text = $@"{_randomTimeleft / 60}:{_randomTimeleft % 60}";
        }

        private void CheckNewGifts()
        {
            var newGiftsCount = _steamgiftsProfileUtils.NewWonGiveawaysCount();

            if (newGiftsCount > 0)
            {
                linkLabelGifts.LinkColor = Color.Red;
            }

            linkLabelGifts.Text = _steamgiftsProfileUtils.NewWonGiveawaysCount().ToString();
        }

        private void backgroundWorkerEnterNextGiveaway_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            _giveawayUtils.EnterGiveaway(_giveawayToEnter);
        }

        private void backgroundWorkerEnterNextGiveaway_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            SetSpinnersVisiblity(true);

            SetLabelsVisibility(false);

            backgroundWorkerRefreshPages.RunWorkerAsync();
        }

        #region StripMenu

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var settingsWindow = new SettingsWindow();
            settingsWindow.Show();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        #endregion

        #region NotifyIcon

        private void MainWindow_Resize(object sender, EventArgs e)
        {
            notifyIcon.BalloonTipTitle = @"AutoJoinSteamgifts minimized";
            notifyIcon.BalloonTipText = @"AutoJoinSteamgifts is now running in background";

            if (WindowState == FormWindowState.Minimized)
            {
                notifyIcon.Visible = true;
                notifyIcon.ShowBalloonTip(500);
                Hide();
            }

            if (WindowState == FormWindowState.Normal)
            {
                notifyIcon.Visible = false;
            }
        }

        private void notifyIcon_Click(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }

        #endregion

        #region ButtonClick
        private void buttonStart_Click(object sender, EventArgs e)
        {
            buttonStart.Enabled = false;
            buttonStop.Enabled = true;

            if (backgroundWorkerRefreshPages.IsBusy) return;

            SetSpinnersVisiblity(true);
            SetLabelsVisibility(false);

            backgroundWorkerRefreshPages.RunWorkerAsync();

            Logger.Trace("Button Start was clicked");
            InitLogger();
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            buttonStop.Enabled = false;
            buttonStart.Enabled = true;

            timerEnterNextGiveaway.Stop();

            Logger.Trace("Button Stop was clicked");
            InitLogger();
        }
        #endregion

        private void StopWithReason(string reason)
        {
            labelStatusValue.Text = reason;

            buttonStart.Enabled = true;
            buttonStop.Enabled = false;

            if (!timerRestart.Enabled)
            {
                timerRestart.Start();
            }

            timerEnterNextGiveaway.Stop();

            Logger.Trace("Application has entered in standby mode. Reason: " + reason + " (Resuming after " + timerRestart.Interval / 3600000 + "h)");
            InitLogger();
        }

        private void InitLogger()
        {
            var richTextBoxContent = "";

            if (File.Exists(Application.StartupPath + "\\logs\\Trace.log"))
            {
                foreach (var line in File.ReadAllLines(Application.StartupPath + "\\logs\\Trace.log").Reverse()
                    .Take(15))
                {
                    richTextBoxContent += line;
                    richTextBoxContent += '\n';
                }
            }
            else
            {
                richTextBoxContent = "Log file is either missing or empty (" + Application.StartupPath + "\\logs\\Trace.log" + ")";
            }

            richTextBoxLog.Invoke(new Action(() => richTextBoxLog.Text = richTextBoxContent));
        }

        #region Delegate events
        private void GiveawayUtilsOnFailedToEnterGiveaway(object sender, EventArgs eventArgs)
        {
            StopWithReason(@"Failed to enter next giveaway");

            backgroundWorkerEnterNextGiveaway.CancelAsync();
        }

        private void GiveawayUtilsOnNewLogMessage(object sender, EventArgs eventArgs)
        {
            InitLogger();
        }
        #endregion

        #region LinkLabelClick
        private void linkLabelGifts_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(Constants.SteamgiftsWonPageUrl);
        }

        private void linkLabelName_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(_steamgiftsProfileUtils.GetProfileLink());
        }
        #endregion

        private void MainWindow_Load(object sender, EventArgs e)
        {
            Logger.Trace("Successfully logged in");
            InitLogger();
        }

        private void timerRestart_Tick(object sender, EventArgs e)
        {
            buttonStart.Enabled = false;
            buttonStop.Enabled = true;

            if (backgroundWorkerRefreshPages.IsBusy) return;

            SetSpinnersVisiblity(true);
            SetLabelsVisibility(false);

            backgroundWorkerRefreshPages.RunWorkerAsync();

            Logger.Trace("Application restarted after 1 hour of standby step");
            InitLogger();
        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                var confirmResult = MessageBox.Show(@"Are you sure you want to exit?",
                    @"Exit confirmation",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

                if (confirmResult == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }
    }
}
