﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using AutoJoinSteamgifts.Managers;
using AutoJoinSteamgifts.Properties;
using AutoJoinSteamgifts.Utils.Steamgifts;
using NLog;

namespace AutoJoinSteamgifts.Windows
{
    public partial class LoginWindow : Form
    {
        private readonly SteamgiftsUtils _steamgiftsUtils = new SteamgiftsUtils();
        private bool _isLoggedIn = false;
        private int _tickCount = 1;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public LoginWindow()
        {
            InitializeComponent();

            Logger.Trace("Application started");

            backgroundWorkerLogin.RunWorkerAsync();
        }

        private void backgroundWorkerLogin_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            CheckForAnotherInstance();

            _steamgiftsUtils.RefreshPage();

            _isLoggedIn = _steamgiftsUtils.IsLoggedIn();

            if (_isLoggedIn)
            {
                Settings.Default.ProfileLink = _steamgiftsUtils.GetProfileLink();
            }
        }

        private void CheckForAnotherInstance()
        {
            var exists = System.Diagnostics.Process
                             .GetProcessesByName(
                                 System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly
                                     .GetEntryAssembly().Location))
                             .Count() > 1;

            if (exists)
            {
                MessageBox.Show(@"Another instance of this application is already running!");
                Application.Exit();
            }
        }

        private void backgroundWorkerLogin_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            pictureBoxSpinner.Hide();

            if (_isLoggedIn)
            {
                labelStatus.Location = new Point(45, 22);
                labelStatus.Text = @"Login OK! You are being redirected.";

                timerLogin.Start();
            }
            else
            {
                labelStatus.Location = new Point(45, 22);
                labelStatus.Text = @"You are not logged in, searching cookies in browsers";

                CookiesManager.GetFreshCookies();

                backgroundWorkerLogin.RunWorkerAsync();

                //labelStatus.Hide();

                //buttonLogin.Visible = true;
            }
        }

        private void timerLogin_Tick(object sender, EventArgs e)
        {
            _tickCount++;

            if (_tickCount <= 3)
            {
                labelStatus.Text = labelStatus.Text + @".";
            }
            else
            {
                timerLogin.Stop();

                var mainWindow = new MainWindow();
                mainWindow.Closed += MainWindowOnClosed;
                mainWindow.Show();

                Hide();
            }
        }

        private void MainWindowOnClosed(object sender, EventArgs eventArgs)
        {
            Close();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            var loginBrowser = new LoginBrowserWindow();
            loginBrowser.Closed += LoginBrowserOnClosed;
            loginBrowser.Show();
        }

        private void LoginBrowserOnClosed(object sender, EventArgs eventArgs)
        {
            backgroundWorkerLogin.RunWorkerAsync();
        }
    }
}
