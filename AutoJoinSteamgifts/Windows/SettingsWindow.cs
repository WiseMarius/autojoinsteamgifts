﻿using System;
using System.Windows.Forms;
using AutoJoinSteamgifts.Properties;
using Microsoft.Win32;

namespace AutoJoinSteamgifts.Windows
{
    public partial class SettingsWindow : Form
    {
        private readonly RegistryKey _registryKey = Registry.CurrentUser.OpenSubKey
            ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

        public SettingsWindow()
        {
            InitializeComponent();

            checkBoxStartWithWindows.Checked = _registryKey.GetValue("AutoJoinSteamgifts") != null;

            numericUpDownCheapestPrice.Value = Settings.Default.CheapestPriceToEnter;
            numericUpDownMaxIntervalGiveaways.Value = Settings.Default.MaxIntervalGiveaways;
            numericUpDownMinIntervalGiveaways.Value = Settings.Default.MinIntervalGiveaways;
            numericUpDownIdleTime.Value = Settings.Default.IdleTime;
        }

        private void CheckBoxStartWithWindows_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxStartWithWindows.Checked)
                _registryKey?.SetValue("AutoJoinSteamgifts", Application.ExecutablePath.ToString());
            else
                _registryKey?.DeleteValue("AutoJoinSteamgifts", false);
        }

        private void ButtonSave_Click(object sender, EventArgs e)
        {
            Settings.Default.CheapestPriceToEnter = numericUpDownCheapestPrice.Value;
            Settings.Default.MaxIntervalGiveaways = numericUpDownMaxIntervalGiveaways.Value;
            Settings.Default.MinIntervalGiveaways = numericUpDownMinIntervalGiveaways.Value;
            Settings.Default.IdleTime = numericUpDownIdleTime.Value;

            Settings.Default.Save();
            Close();
        }

        private void ButtonResetDefault_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show(@"Are you sure you want to reset all settings to the default values?",
                @"Reset default confirmation",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (confirmResult == DialogResult.Yes)
            {
                numericUpDownCheapestPrice.Value = 20;
                numericUpDownMaxIntervalGiveaways.Value = 300;
                numericUpDownMinIntervalGiveaways.Value = 5;
                numericUpDownIdleTime.Value = 1;
            }
        }
    }
}
