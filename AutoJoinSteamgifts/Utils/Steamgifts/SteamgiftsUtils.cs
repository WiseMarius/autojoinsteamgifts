﻿using System;
using System.Linq;
using AutoJoinSteamgifts.Filters;
using AutoJoinSteamgifts.Models;

namespace AutoJoinSteamgifts.Utils.Steamgifts
{
    class SteamgiftsUtils : GiveawaysFilter
    {
        private int _pageNumber = 1;
        
        public SteamgiftsUtils()
        {
            PageAddress = Constants.SteamgiftsUrl + Constants.SteamgiftsPageSufix + _pageNumber;
        }

        public bool IsLoggedIn()
        {
            return HtmlDocument.DocumentNode.SelectSingleNode("//div[@class='nav__avatar-inner-wrap']") != null;
        }

        public string GetPoints()
        {
            return HtmlDocument.DocumentNode.SelectSingleNode("//span[@class='nav__points']").InnerText;
        }

        public Giveaway GetNextGiveaway()
        {
            var giveawayNumber = 1;
            var node = HtmlDocument.DocumentNode.SelectSingleNode("//div[@class='giveaway__row-outer-wrap']" + "[" + giveawayNumber + "]");
            var giveaway = new Giveaway();

            while (!MeetsAllRequirements(node))
            {
                giveawayNumber++;

                if (giveawayNumber == GetResultsOnPage())
                {
                    _pageNumber++;
                    giveawayNumber = 1;

                    PageAddress = Constants.SteamgiftsUrl + Constants.SteamgiftsPageSufix + _pageNumber;
                    RefreshPage();
                }

                node = HtmlDocument.DocumentNode.SelectSingleNode("//div[@class='giveaway__row-outer-wrap']" + "[" + giveawayNumber + "]");
            }

            giveaway.Name = node.SelectSingleNode(".//a[@class='giveaway__heading__name']").InnerHtml;
            giveaway.Link = node.SelectSingleNode(".//a[@class='giveaway__heading__name']").Attributes[1].Value;
            giveaway.TimeToEnter = GetTimeleftInSeconds(node) - Constants.MarginOfErrorInSeconds;
            var numberOfHeadings = node.SelectNodes(".//span[@class='giveaway__heading__thin']").Count;
            giveaway.Points = int.Parse(new string(node.SelectNodes(".//span[@class='giveaway__heading__thin']")[numberOfHeadings - 1].InnerText.ToCharArray().Where(char.IsDigit).ToArray()));

            return giveaway;
        }

        private int GetResultsOnPage()
        {
            return int.Parse(HtmlDocument.DocumentNode.SelectSingleNode("//div[@class='pagination__results']")
                .ChildNodes[3].InnerHtml);
        }
    }
}
