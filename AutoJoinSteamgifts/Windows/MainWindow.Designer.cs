﻿namespace AutoJoinSteamgifts.Windows
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.labelPoints = new System.Windows.Forms.Label();
            this.labelPointsVaue = new System.Windows.Forms.Label();
            this.linkLabelName = new System.Windows.Forms.LinkLabel();
            this.labelName = new System.Windows.Forms.Label();
            this.labelNextEntrance = new System.Windows.Forms.Label();
            this.labelGiveaway = new System.Windows.Forms.Label();
            this.labelGiveawayValue = new System.Windows.Forms.Label();
            this.labelTime = new System.Windows.Forms.Label();
            this.labelTimeValue = new System.Windows.Forms.Label();
            this.pictureBoxSpinner1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxSpinner2 = new System.Windows.Forms.PictureBox();
            this.timerEnterNextGiveaway = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorkerRefreshPages = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerGetNextGiveaway = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerEnterNextGiveaway = new System.ComponentModel.BackgroundWorker();
            this.pictureBoxSpinner3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxSpinner4 = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.labelStatus = new System.Windows.Forms.Label();
            this.labelStatusValue = new System.Windows.Forms.Label();
            this.pictureBoxSpinner5 = new System.Windows.Forms.PictureBox();
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.richTextBoxLog = new System.Windows.Forms.RichTextBox();
            this.labelLog = new System.Windows.Forms.Label();
            this.labelGifts = new System.Windows.Forms.Label();
            this.linkLabelGifts = new System.Windows.Forms.LinkLabel();
            this.pictureBoxSpinner6 = new System.Windows.Forms.PictureBox();
            this.timerRestart = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpinner1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpinner2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpinner3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpinner4)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpinner5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpinner6)).BeginInit();
            this.SuspendLayout();
            // 
            // labelPoints
            // 
            this.labelPoints.Location = new System.Drawing.Point(12, 46);
            this.labelPoints.Name = "labelPoints";
            this.labelPoints.Size = new System.Drawing.Size(47, 16);
            this.labelPoints.TabIndex = 0;
            this.labelPoints.Text = "Points:";
            // 
            // labelPointsVaue
            // 
            this.labelPointsVaue.Location = new System.Drawing.Point(57, 46);
            this.labelPointsVaue.Name = "labelPointsVaue";
            this.labelPointsVaue.Size = new System.Drawing.Size(45, 16);
            this.labelPointsVaue.TabIndex = 1;
            this.labelPointsVaue.Text = "400";
            this.labelPointsVaue.Visible = false;
            // 
            // linkLabelName
            // 
            this.linkLabelName.Location = new System.Drawing.Point(57, 26);
            this.linkLabelName.Name = "linkLabelName";
            this.linkLabelName.Size = new System.Drawing.Size(45, 16);
            this.linkLabelName.TabIndex = 2;
            this.linkLabelName.TabStop = true;
            this.linkLabelName.Text = "Name";
            this.linkLabelName.Visible = false;
            this.linkLabelName.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelName_LinkClicked);
            // 
            // labelName
            // 
            this.labelName.Location = new System.Drawing.Point(12, 26);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(47, 16);
            this.labelName.TabIndex = 3;
            this.labelName.Text = "Name:";
            // 
            // labelNextEntrance
            // 
            this.labelNextEntrance.AutoSize = true;
            this.labelNextEntrance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNextEntrance.Location = new System.Drawing.Point(370, 23);
            this.labelNextEntrance.Name = "labelNextEntrance";
            this.labelNextEntrance.Size = new System.Drawing.Size(87, 13);
            this.labelNextEntrance.TabIndex = 4;
            this.labelNextEntrance.Text = "Next entrance";
            // 
            // labelGiveaway
            // 
            this.labelGiveaway.AutoSize = true;
            this.labelGiveaway.Location = new System.Drawing.Point(346, 43);
            this.labelGiveaway.Name = "labelGiveaway";
            this.labelGiveaway.Size = new System.Drawing.Size(57, 13);
            this.labelGiveaway.TabIndex = 5;
            this.labelGiveaway.Text = "Giveaway:";
            // 
            // labelGiveawayValue
            // 
            this.labelGiveawayValue.AutoSize = true;
            this.labelGiveawayValue.Location = new System.Drawing.Point(406, 43);
            this.labelGiveawayValue.Name = "labelGiveawayValue";
            this.labelGiveawayValue.Size = new System.Drawing.Size(92, 13);
            this.labelGiveawayValue.TabIndex = 6;
            this.labelGiveawayValue.Text = "Some game name";
            this.labelGiveawayValue.Visible = false;
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.Location = new System.Drawing.Point(370, 56);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(33, 13);
            this.labelTime.TabIndex = 7;
            this.labelTime.Text = "Time:";
            // 
            // labelTimeValue
            // 
            this.labelTimeValue.AutoSize = true;
            this.labelTimeValue.Location = new System.Drawing.Point(406, 57);
            this.labelTimeValue.Name = "labelTimeValue";
            this.labelTimeValue.Size = new System.Drawing.Size(34, 13);
            this.labelTimeValue.TabIndex = 8;
            this.labelTimeValue.Text = "00:00";
            this.labelTimeValue.Visible = false;
            // 
            // pictureBoxSpinner1
            // 
            this.pictureBoxSpinner1.Image = global::AutoJoinSteamgifts.Properties.Resources.imgSpinner;
            this.pictureBoxSpinner1.Location = new System.Drawing.Point(56, 46);
            this.pictureBoxSpinner1.Name = "pictureBoxSpinner1";
            this.pictureBoxSpinner1.Size = new System.Drawing.Size(15, 16);
            this.pictureBoxSpinner1.TabIndex = 9;
            this.pictureBoxSpinner1.TabStop = false;
            this.pictureBoxSpinner1.Visible = false;
            // 
            // pictureBoxSpinner2
            // 
            this.pictureBoxSpinner2.Image = global::AutoJoinSteamgifts.Properties.Resources.imgSpinner;
            this.pictureBoxSpinner2.Location = new System.Drawing.Point(56, 26);
            this.pictureBoxSpinner2.Name = "pictureBoxSpinner2";
            this.pictureBoxSpinner2.Size = new System.Drawing.Size(15, 16);
            this.pictureBoxSpinner2.TabIndex = 10;
            this.pictureBoxSpinner2.TabStop = false;
            this.pictureBoxSpinner2.Visible = false;
            // 
            // timerEnterNextGiveaway
            // 
            this.timerEnterNextGiveaway.Interval = 1000;
            this.timerEnterNextGiveaway.Tick += new System.EventHandler(this.timerEnterNextGiveaway_Tick);
            // 
            // backgroundWorkerRefreshPages
            // 
            this.backgroundWorkerRefreshPages.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerRefreshPages_DoWork);
            this.backgroundWorkerRefreshPages.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerRefreshPages_RunWorkerCompleted);
            // 
            // backgroundWorkerGetNextGiveaway
            // 
            this.backgroundWorkerGetNextGiveaway.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerGetNextGiveaway_DoWork);
            this.backgroundWorkerGetNextGiveaway.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerGetNextGiveaway_RunWorkerCompleted);
            // 
            // backgroundWorkerEnterNextGiveaway
            // 
            this.backgroundWorkerEnterNextGiveaway.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerEnterNextGiveaway_DoWork);
            this.backgroundWorkerEnterNextGiveaway.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerEnterNextGiveaway_RunWorkerCompleted);
            // 
            // pictureBoxSpinner3
            // 
            this.pictureBoxSpinner3.Image = global::AutoJoinSteamgifts.Properties.Resources.imgSpinner;
            this.pictureBoxSpinner3.Location = new System.Drawing.Point(409, 42);
            this.pictureBoxSpinner3.Name = "pictureBoxSpinner3";
            this.pictureBoxSpinner3.Size = new System.Drawing.Size(15, 16);
            this.pictureBoxSpinner3.TabIndex = 11;
            this.pictureBoxSpinner3.TabStop = false;
            this.pictureBoxSpinner3.Visible = false;
            // 
            // pictureBoxSpinner4
            // 
            this.pictureBoxSpinner4.Image = global::AutoJoinSteamgifts.Properties.Resources.imgSpinner;
            this.pictureBoxSpinner4.Location = new System.Drawing.Point(409, 57);
            this.pictureBoxSpinner4.Name = "pictureBoxSpinner4";
            this.pictureBoxSpinner4.Size = new System.Drawing.Size(15, 16);
            this.pictureBoxSpinner4.TabIndex = 12;
            this.pictureBoxSpinner4.TabStop = false;
            this.pictureBoxSpinner4.Visible = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(584, 24);
            this.menuStrip1.TabIndex = 13;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem});
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.testToolStripMenuItem.Text = "File";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Image = global::AutoJoinSteamgifts.Properties.Resources.imgSettings;
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Image = global::AutoJoinSteamgifts.Properties.Resources.imgInfo;
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // notifyIcon
            // 
            this.notifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "notifyIcon1";
            this.notifyIcon.BalloonTipClicked += new System.EventHandler(this.notifyIcon_Click);
            this.notifyIcon.Click += new System.EventHandler(this.notifyIcon_Click);
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(118, 23);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(40, 13);
            this.labelStatus.TabIndex = 14;
            this.labelStatus.Text = "Status:";
            // 
            // labelStatusValue
            // 
            this.labelStatusValue.AutoSize = true;
            this.labelStatusValue.Location = new System.Drawing.Point(162, 23);
            this.labelStatusValue.Name = "labelStatusValue";
            this.labelStatusValue.Size = new System.Drawing.Size(47, 13);
            this.labelStatusValue.TabIndex = 15;
            this.labelStatusValue.Text = "Working";
            this.labelStatusValue.Visible = false;
            // 
            // pictureBoxSpinner5
            // 
            this.pictureBoxSpinner5.Image = global::AutoJoinSteamgifts.Properties.Resources.imgSpinner;
            this.pictureBoxSpinner5.Location = new System.Drawing.Point(162, 23);
            this.pictureBoxSpinner5.Name = "pictureBoxSpinner5";
            this.pictureBoxSpinner5.Size = new System.Drawing.Size(15, 16);
            this.pictureBoxSpinner5.TabIndex = 16;
            this.pictureBoxSpinner5.TabStop = false;
            this.pictureBoxSpinner5.Visible = false;
            // 
            // buttonStart
            // 
            this.buttonStart.Enabled = false;
            this.buttonStart.Location = new System.Drawing.Point(121, 43);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 17;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // buttonStop
            // 
            this.buttonStop.Location = new System.Drawing.Point(202, 43);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(75, 23);
            this.buttonStop.TabIndex = 18;
            this.buttonStop.Text = "Stop";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // richTextBoxLog
            // 
            this.richTextBoxLog.Enabled = false;
            this.richTextBoxLog.Location = new System.Drawing.Point(15, 96);
            this.richTextBoxLog.Name = "richTextBoxLog";
            this.richTextBoxLog.Size = new System.Drawing.Size(557, 215);
            this.richTextBoxLog.TabIndex = 19;
            this.richTextBoxLog.Text = "";
            // 
            // labelLog
            // 
            this.labelLog.AutoSize = true;
            this.labelLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLog.Location = new System.Drawing.Point(249, 80);
            this.labelLog.Name = "labelLog";
            this.labelLog.Size = new System.Drawing.Size(28, 13);
            this.labelLog.TabIndex = 20;
            this.labelLog.Text = "Log";
            // 
            // labelGifts
            // 
            this.labelGifts.Location = new System.Drawing.Point(12, 66);
            this.labelGifts.Name = "labelGifts";
            this.labelGifts.Size = new System.Drawing.Size(47, 16);
            this.labelGifts.TabIndex = 21;
            this.labelGifts.Text = "Gifts:";
            // 
            // linkLabelGifts
            // 
            this.linkLabelGifts.LinkColor = System.Drawing.Color.Black;
            this.linkLabelGifts.Location = new System.Drawing.Point(56, 66);
            this.linkLabelGifts.Name = "linkLabelGifts";
            this.linkLabelGifts.Size = new System.Drawing.Size(46, 16);
            this.linkLabelGifts.TabIndex = 22;
            this.linkLabelGifts.TabStop = true;
            this.linkLabelGifts.Text = "0";
            this.linkLabelGifts.Visible = false;
            this.linkLabelGifts.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelGifts_LinkClicked);
            // 
            // pictureBoxSpinner6
            // 
            this.pictureBoxSpinner6.Image = global::AutoJoinSteamgifts.Properties.Resources.imgSpinner;
            this.pictureBoxSpinner6.Location = new System.Drawing.Point(56, 66);
            this.pictureBoxSpinner6.Name = "pictureBoxSpinner6";
            this.pictureBoxSpinner6.Size = new System.Drawing.Size(15, 16);
            this.pictureBoxSpinner6.TabIndex = 23;
            this.pictureBoxSpinner6.TabStop = false;
            this.pictureBoxSpinner6.Visible = false;
            // 
            // timerRestart
            // 
            this.timerRestart.Interval = 3600000;
            this.timerRestart.Tick += new System.EventHandler(this.timerRestart_Tick);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 328);
            this.Controls.Add(this.pictureBoxSpinner6);
            this.Controls.Add(this.linkLabelGifts);
            this.Controls.Add(this.labelGifts);
            this.Controls.Add(this.labelLog);
            this.Controls.Add(this.richTextBoxLog);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.pictureBoxSpinner5);
            this.Controls.Add(this.labelStatusValue);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.pictureBoxSpinner4);
            this.Controls.Add(this.pictureBoxSpinner3);
            this.Controls.Add(this.pictureBoxSpinner2);
            this.Controls.Add(this.pictureBoxSpinner1);
            this.Controls.Add(this.labelTimeValue);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.labelGiveawayValue);
            this.Controls.Add(this.labelGiveaway);
            this.Controls.Add(this.labelNextEntrance);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.linkLabelName);
            this.Controls.Add(this.labelPointsVaue);
            this.Controls.Add(this.labelPoints);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainWindow";
            this.Text = "AutoJoin Steamgifts";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.Resize += new System.EventHandler(this.MainWindow_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpinner1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpinner2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpinner3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpinner4)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpinner5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpinner6)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelPoints;
        private System.Windows.Forms.Label labelPointsVaue;
        private System.Windows.Forms.LinkLabel linkLabelName;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelNextEntrance;
        private System.Windows.Forms.Label labelGiveaway;
        private System.Windows.Forms.Label labelGiveawayValue;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Label labelTimeValue;
        private System.Windows.Forms.PictureBox pictureBoxSpinner1;
        private System.Windows.Forms.PictureBox pictureBoxSpinner2;
        private System.Windows.Forms.Timer timerEnterNextGiveaway;
        private System.ComponentModel.BackgroundWorker backgroundWorkerRefreshPages;
        private System.ComponentModel.BackgroundWorker backgroundWorkerGetNextGiveaway;
        private System.ComponentModel.BackgroundWorker backgroundWorkerEnterNextGiveaway;
        private System.Windows.Forms.PictureBox pictureBoxSpinner3;
        private System.Windows.Forms.PictureBox pictureBoxSpinner4;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Label labelStatusValue;
        private System.Windows.Forms.PictureBox pictureBoxSpinner5;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.RichTextBox richTextBoxLog;
        private System.Windows.Forms.Label labelLog;
        private System.Windows.Forms.Label labelGifts;
        private System.Windows.Forms.LinkLabel linkLabelGifts;
        private System.Windows.Forms.PictureBox pictureBoxSpinner6;
        private System.Windows.Forms.Timer timerRestart;
    }
}

