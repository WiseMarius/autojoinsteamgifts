﻿using System.Windows.Forms;
using AutoJoinSteamgifts.Managers;
using AutoJoinSteamgifts.Utils.Steamgifts;

namespace AutoJoinSteamgifts.Windows
{
    public partial class LoginBrowserWindow : Form
    {
        private readonly SteamgiftsUtils _steamgiftsUtils = new SteamgiftsUtils();

        public LoginBrowserWindow()
        {
            InitializeComponent();

            loginWebBrowser.Navigate(Constants.SteamgiftsUrlLogin);
        }

        private void DocumentCompleted_LoginWebBrowser(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            var url = loginWebBrowser.Url.AbsoluteUri;
            if (url != Constants.SteamgiftsUrl || _steamgiftsUtils.IsLoggedIn()) return;

            CookiesManager.GetFreshCookies();
            Close();
        }
    }
}
