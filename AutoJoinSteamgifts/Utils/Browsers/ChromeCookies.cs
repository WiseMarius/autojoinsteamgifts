﻿using System;
using System.IO;
using System.Data.SQLite;
using System.Text;

namespace AutoJoinSteamgifts.Utils.Browsers
{
    class ChromeCookies : IBrowserCookies
    {
        public Tuple<string, string> GetCookies()
        {
            var path = GetChromeCookiePath();

            if (path != string.Empty) 
            {
                try
                {
                    var database = "Data Source=" + path + ";pooling=false";

                    using (var connection = new SQLiteConnection(database))
                    {
                        using (SQLiteCommand command = connection.CreateCommand())
                        {
                            command.CommandText = "select encrypted_value, name from cookies where name = 'PHPSESSID' and host_key like '%steam%'";

                            connection.Open();

                            using (SQLiteDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    var encryptedData = (byte[])reader[0];
                                    var decodedData = System.Security.Cryptography.ProtectedData.Unprotect(encryptedData, null, System.Security.Cryptography.DataProtectionScope.CurrentUser);
                                    var plainText = Encoding.ASCII.GetString(decodedData);

                                    var cookieName = reader[1].ToString();

                                    return Tuple.Create(cookieName, plainText);
                                }
                            }

                            connection.Close();
                        }
                    }
                }
                catch(Exception)
                {

                }
            }

            return Tuple.Create(string.Empty, string.Empty);
        }

        private static string GetChromeCookiePath()
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            path += @"\Google\Chrome\User Data\Default\cookies";

            return !File.Exists(path) ? string.Empty : path;
        }
    }
}
