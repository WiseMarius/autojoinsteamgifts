﻿namespace AutoJoinSteamgifts.Windows
{
    partial class SettingsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsWindow));
            this.checkBoxStartWithWindows = new System.Windows.Forms.CheckBox();
            this.numericUpDownCheapestPrice = new System.Windows.Forms.NumericUpDown();
            this.labelCheapestPrice = new System.Windows.Forms.Label();
            this.numericUpDownMinIntervalGiveaways = new System.Windows.Forms.NumericUpDown();
            this.labelMinIntervalGiveaways = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDownMaxIntervalGiveaways = new System.Windows.Forms.NumericUpDown();
            this.buttonSave = new System.Windows.Forms.Button();
            this.labelIdleTime = new System.Windows.Forms.Label();
            this.numericUpDownIdleTime = new System.Windows.Forms.NumericUpDown();
            this.buttonResetDefault = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCheapestPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinIntervalGiveaways)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxIntervalGiveaways)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIdleTime)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBoxStartWithWindows
            // 
            this.checkBoxStartWithWindows.AutoSize = true;
            this.checkBoxStartWithWindows.Location = new System.Drawing.Point(15, 10);
            this.checkBoxStartWithWindows.Name = "checkBoxStartWithWindows";
            this.checkBoxStartWithWindows.Size = new System.Drawing.Size(171, 17);
            this.checkBoxStartWithWindows.TabIndex = 1;
            this.checkBoxStartWithWindows.Text = "Start application with Windows";
            this.checkBoxStartWithWindows.UseVisualStyleBackColor = true;
            this.checkBoxStartWithWindows.CheckedChanged += new System.EventHandler(this.CheckBoxStartWithWindows_CheckedChanged);
            // 
            // numericUpDownCheapestPrice
            // 
            this.numericUpDownCheapestPrice.Location = new System.Drawing.Point(15, 30);
            this.numericUpDownCheapestPrice.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericUpDownCheapestPrice.Name = "numericUpDownCheapestPrice";
            this.numericUpDownCheapestPrice.Size = new System.Drawing.Size(42, 20);
            this.numericUpDownCheapestPrice.TabIndex = 2;
            this.numericUpDownCheapestPrice.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // labelCheapestPrice
            // 
            this.labelCheapestPrice.Location = new System.Drawing.Point(63, 30);
            this.labelCheapestPrice.Name = "labelCheapestPrice";
            this.labelCheapestPrice.Size = new System.Drawing.Size(337, 20);
            this.labelCheapestPrice.TabIndex = 3;
            this.labelCheapestPrice.Text = "Cheapest price to enter";
            this.labelCheapestPrice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numericUpDownMinIntervalGiveaways
            // 
            this.numericUpDownMinIntervalGiveaways.Location = new System.Drawing.Point(15, 55);
            this.numericUpDownMinIntervalGiveaways.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericUpDownMinIntervalGiveaways.Name = "numericUpDownMinIntervalGiveaways";
            this.numericUpDownMinIntervalGiveaways.Size = new System.Drawing.Size(42, 20);
            this.numericUpDownMinIntervalGiveaways.TabIndex = 4;
            this.numericUpDownMinIntervalGiveaways.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // labelMinIntervalGiveaways
            // 
            this.labelMinIntervalGiveaways.Location = new System.Drawing.Point(63, 55);
            this.labelMinIntervalGiveaways.Name = "labelMinIntervalGiveaways";
            this.labelMinIntervalGiveaways.Size = new System.Drawing.Size(340, 20);
            this.labelMinIntervalGiveaways.TabIndex = 5;
            this.labelMinIntervalGiveaways.Text = "Minimum seconds to wait between giveaways";
            this.labelMinIntervalGiveaways.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(63, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(340, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Maximum seconds to wait between giveaways";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numericUpDownMaxIntervalGiveaways
            // 
            this.numericUpDownMaxIntervalGiveaways.Location = new System.Drawing.Point(15, 80);
            this.numericUpDownMaxIntervalGiveaways.Maximum = new decimal(new int[] {
            3600,
            0,
            0,
            0});
            this.numericUpDownMaxIntervalGiveaways.Minimum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericUpDownMaxIntervalGiveaways.Name = "numericUpDownMaxIntervalGiveaways";
            this.numericUpDownMaxIntervalGiveaways.Size = new System.Drawing.Size(42, 20);
            this.numericUpDownMaxIntervalGiveaways.TabIndex = 6;
            this.numericUpDownMaxIntervalGiveaways.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(347, 126);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 8;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.ButtonSave_Click);
            // 
            // labelIdleTime
            // 
            this.labelIdleTime.Location = new System.Drawing.Point(63, 105);
            this.labelIdleTime.Name = "labelIdleTime";
            this.labelIdleTime.Size = new System.Drawing.Size(340, 20);
            this.labelIdleTime.TabIndex = 10;
            this.labelIdleTime.Text = "Idle time (in hours) after remaining without points";
            this.labelIdleTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numericUpDownIdleTime
            // 
            this.numericUpDownIdleTime.Cursor = System.Windows.Forms.Cursors.Default;
            this.numericUpDownIdleTime.DecimalPlaces = 1;
            this.numericUpDownIdleTime.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownIdleTime.Location = new System.Drawing.Point(15, 105);
            this.numericUpDownIdleTime.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericUpDownIdleTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownIdleTime.Name = "numericUpDownIdleTime";
            this.numericUpDownIdleTime.Size = new System.Drawing.Size(42, 20);
            this.numericUpDownIdleTime.TabIndex = 9;
            this.numericUpDownIdleTime.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // buttonResetDefault
            // 
            this.buttonResetDefault.Location = new System.Drawing.Point(256, 126);
            this.buttonResetDefault.Name = "buttonResetDefault";
            this.buttonResetDefault.Size = new System.Drawing.Size(85, 23);
            this.buttonResetDefault.TabIndex = 11;
            this.buttonResetDefault.Text = "Reset default";
            this.buttonResetDefault.UseVisualStyleBackColor = true;
            this.buttonResetDefault.Click += new System.EventHandler(this.ButtonResetDefault_Click);
            // 
            // SettingsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 161);
            this.Controls.Add(this.buttonResetDefault);
            this.Controls.Add(this.labelIdleTime);
            this.Controls.Add(this.numericUpDownIdleTime);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numericUpDownMaxIntervalGiveaways);
            this.Controls.Add(this.labelMinIntervalGiveaways);
            this.Controls.Add(this.numericUpDownMinIntervalGiveaways);
            this.Controls.Add(this.labelCheapestPrice);
            this.Controls.Add(this.numericUpDownCheapestPrice);
            this.Controls.Add(this.checkBoxStartWithWindows);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SettingsWindow";
            this.Text = "SettingsWindow";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCheapestPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinIntervalGiveaways)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxIntervalGiveaways)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIdleTime)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxStartWithWindows;
        private System.Windows.Forms.NumericUpDown numericUpDownCheapestPrice;
        private System.Windows.Forms.Label labelCheapestPrice;
        private System.Windows.Forms.NumericUpDown numericUpDownMinIntervalGiveaways;
        private System.Windows.Forms.Label labelMinIntervalGiveaways;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxIntervalGiveaways;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Label labelIdleTime;
        private System.Windows.Forms.NumericUpDown numericUpDownIdleTime;
        private System.Windows.Forms.Button buttonResetDefault;
    }
}