﻿using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using AutoJoinSteamgifts.Managers;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;

namespace AutoJoinSteamgifts.Utils
{
    class PageUtils
    {
        protected string PageAddress;
        protected HtmlDocument HtmlDocument = new HtmlDocument();

        public void RefreshPage()
        {
            HtmlDocument = GetPage(PageAddress);
        }

        protected HtmlDocument GetPage(string url)
        {
            if (IsInternet())
            {
                var htmlDocument = new HtmlDocument();
                var webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.CookieContainer = CookiesManager.GetCookies();

                var webResponse = webRequest.GetResponse();

                using (var stream = webResponse.GetResponseStream())
                {
                    var reader = new StreamReader(stream, Encoding.UTF8);
                    var html = reader.ReadToEnd();

                    htmlDocument.LoadHtml(html);

                    File.WriteAllText(@"D:\sg.html", html);
                }

                return htmlDocument;
            }

            var button = MessageBox.Show(@"No internet connection", @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            if (button == DialogResult.OK)
            {
                Application.Exit();
            }

            return null;
        }

        private static bool IsInternet()
        {
            try
            {
                using (var client = new WebClient())
                {
                    using (client.OpenRead("http://google.ro"))
                    {
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
