﻿namespace AutoJoinSteamgifts.Windows
{
    partial class LoginBrowserWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginBrowserWindow));
            this.loginWebBrowser = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // loginWebBrowser
            // 
            this.loginWebBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loginWebBrowser.Location = new System.Drawing.Point(0, 0);
            this.loginWebBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.loginWebBrowser.Name = "loginWebBrowser";
            this.loginWebBrowser.ScriptErrorsSuppressed = true;
            this.loginWebBrowser.Size = new System.Drawing.Size(584, 561);
            this.loginWebBrowser.TabIndex = 0;
            this.loginWebBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.DocumentCompleted_LoginWebBrowser);
            // 
            // LoginBrowserWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 561);
            this.Controls.Add(this.loginWebBrowser);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LoginBrowserWindow";
            this.Text = "Login Steamgifts";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser loginWebBrowser;
    }
}